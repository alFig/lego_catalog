package services.db;



public class Colors {
	private int id;
	private String name;
	private String rgb;
	private String isTrans;

	public Colors(int id, String name, String rgb, String isTrans) {
		this.setId(id);
		this.setName(name);
		this.setRgb(rgb);
		this.setIsTrans(isTrans);
	}
	
	public Colors() {
		setId(0);
		setName("");
		setRgb("");
		setIsTrans("");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRgb() {
		return rgb;
	}

	public void setRgb(String rgb) {
		this.rgb = rgb;
	}

	public String getIsTrans() {
		return isTrans;
	}

	public void setIsTrans(String isTrans) {
		this.isTrans = isTrans;
	}



}
