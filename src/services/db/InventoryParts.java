package services.db;


public class InventoryParts {
	private int inventoryId;
	private String partNum;
	private int colorId;
	private int quantity;
	private String isSpare;

	public InventoryParts(int inventoryId, String partNum, int colorId, int quantity, String isSpare) {
		this.inventoryId = inventoryId;
		this.partNum = partNum;
		this.colorId = colorId;
		this.quantity = quantity;
		this.isSpare = isSpare;
	}

	public int getInventoryId() {
		return inventoryId;
	}

	public String getPartNum() {
		return partNum;
	}

	public int getColorId() {
		return colorId;
	}

	public int getQuantity() {
		return quantity;
	}

	public String getIsSpare() {
		return isSpare;
	}

}
