package services.db;


public class Inventories {
	
	private int id;
	private int version;
	private String setNum;
	
	public Inventories(int id, int version, String setNum) {
		this.id = id;
		this.version = version;
		this.setNum = setNum;
	}
	
	public int getId() {
		return id;
	}
	
	public int getVersion() {
		return version;
	}
	
	public String getSetNum() {
		return setNum;
	}

}
