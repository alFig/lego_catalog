package services.db;


public class Sets {
	private String setNum;
	private String name;
	private int year;
	private int themeId;
	private int numParts;
	
	public Sets(String setNum, String name, int year, int themeId, int numParts) {
		this.setSetNum(setNum);
		this.setName(name);
		this.setYear(year);
		this.setThemeId(themeId);
		this.setNumParts(numParts);
	}
	
	public Sets() {
		setSetNum("");
		setName("");
		setYear(0);
		setThemeId(0);
		setNumParts(0);
	}

	public String getSetNum() {
		return setNum;
	}

	public void setSetNum(String setNum) {
		this.setNum = setNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getThemeId() {
		return themeId;
	}

	public void setThemeId(int themeId) {
		this.themeId = themeId;
	}

	public int getNumParts() {
		return numParts;
	}

	public void setNumParts(int numParts) {
		this.numParts = numParts;
	}
	

}
