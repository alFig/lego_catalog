package services.db;


public class Parts {
	private int partNum;
	private String name;
	private String partCatId;
	
	public Parts(int partNum, String name, String partCatId) {
		this.partNum = partNum;
		this.name = name;
		this.partCatId = partCatId;
	}
	
	public int getPartNum() {
		return partNum;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCatId() {
		return partCatId;
	}

}
