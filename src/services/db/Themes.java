package services.db;

public class Themes {
	private int id;
	private String name;
	private int parentId;

	public Themes(int id, String name, int parentId) {
		this.setId(id);
		this.setName(name);
		this.setParentId(parentId);
	}

	public Themes() {
		setId(0);
		setName("");
		setParentId(0);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

}
