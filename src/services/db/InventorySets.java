package services.db;


public class InventorySets {
	
	private int inventoryId;
	private String setNum;
	private int quantity;
	
	public InventorySets(int inventoryId, String setNum, int quantity) {
		this.inventoryId = inventoryId;
		this.setNum = setNum;
		this.quantity = quantity;
	}
	
	public int getInventoryId() {
		return inventoryId;
	}
	
	public String getSetNum() {
		return setNum;
	}
	
	public int getQuantity() {
		return quantity;
	}

}
