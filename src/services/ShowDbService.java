package services;


import java.util.ArrayList;
import java.util.List;

import dao.DaoException;
import dao.DaoFactory;
import dao.sqlite.SetsSQLite;
import dao.sqlite.ThemesSQLite;
import services.db.Sets;
import services.db.Themes;

public class ShowDbService {
	private DaoFactory daoFactory;
	private ThemesSQLite themesSQLite;
	private SetsSQLite setsSQLite ;
		
	public ShowDbService() throws DaoException {
		daoFactory = DaoFactory.getInstance();
		themesSQLite = daoFactory.getThemesDao();
		setsSQLite = daoFactory.getSetsDao();
		
	}
		
	public List<ArrayList<String>> showThems() throws DaoException {
		List<Themes> list = null;
		ArrayList<String> row = new ArrayList<String>();
		List<ArrayList<String>> rowsList = new ArrayList<ArrayList<String>>();

		row.add("id");
		row.add("Name");
		row.add("parent id");
		rowsList.add(row);
		
		list = themesSQLite.selectAll();
		for (Themes themes : list) {
			row = new ArrayList<String>();
			row.add(Integer.toString(themes.getId()));
			row.add(themes.getName().trim());
			row.add(Integer.toString(themes.getParentId()));
			rowsList.add(row);
		}

		return rowsList;
	}
	
	public List<ArrayList<String>> showSets() throws DaoException {
		List<Sets> list = null;
		ArrayList<String> row = new ArrayList<String>();
		List<ArrayList<String>> rowsList = new ArrayList<ArrayList<String>>();

		row.add("id");
		row.add("Name");
		row.add("Year");
		row.add("id theme");
		row.add("Num parts");
		rowsList.add(row);
		
		list = setsSQLite.selectAll();
		for (Sets sets : list) {
			row = new ArrayList<String>();
			row.add(sets.getSetNum());
			row.add(sets.getName().trim());
			row.add(Integer.toString(sets.getYear()));
			row.add(Integer.toString(sets.getThemeId()));
			row.add(Integer.toString(sets.getNumParts()));
			rowsList.add(row);
		}
		
		return rowsList;
	}
	

}
