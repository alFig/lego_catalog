package services;

import java.util.List;

import dao.DaoException;
import dao.DaoFactory;
import dao.sqlite.DbTools;
import dao.sqlite.SetsSQLite;
import dao.sqlite.ThemesSQLite;
import services.db.Sets;
import services.db.Themes;
import services.loadcsv.CsvConverter;
import services.loadcsv.CsvLoader;
import services.loadcsv.SetsCsvConverter;
import services.loadcsv.ThemesCsvConverter;

public class LoadDbService {
	private DbTools dbTools;
	private DaoFactory daoFactory;
	private ThemesSQLite themesSQLite;
	private SetsSQLite setsSQLite;
	private CsvLoader csvLoader;

	public LoadDbService() throws DaoException {
		daoFactory = DaoFactory.getInstance();
		csvLoader = CsvLoader.getInstance();
		themesSQLite = daoFactory.getThemesDao();
		setsSQLite = daoFactory.getSetsDao();
		dbTools = new DbTools();

	}

	private void loadThemes() throws DaoException {
		String filePatch = "E:\\Project\\java_2\\DAO\\CSV_file\\themes.csv";
		List<Themes> resultList = null;

		// clear the table
		themesSQLite.dropTable(dbTools);
		themesSQLite.createTable(dbTools);

		// load the table
		CsvConverter<Themes> converter = new ThemesCsvConverter();
		resultList = csvLoader.load(filePatch, converter);
		for (Themes themes : resultList) {
			themesSQLite.create(themes);
		}
	}

	private void loadSets() throws DaoException {
		String filePatch = "E:\\Project\\java_2\\DAO\\CSV_file\\sets.csv";
		List<Sets> resultList = null;

		// clear the table
		setsSQLite.dropTable(dbTools);
		setsSQLite.createTable(dbTools);

		// load the table
		CsvConverter<Sets> converter = new SetsCsvConverter();
		resultList = csvLoader.load(filePatch, converter);
		for (Sets sets : resultList) {
			setsSQLite.create(sets);
		}
	}

	public void createDB() throws DaoException {
		loadThemes();
		loadSets();

	}

}
