package services.loadcsv;

import java.util.List;

public interface CsvConverter<T> {

	public T convert(List<String> headers, List<String> row);

}
