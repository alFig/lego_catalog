package services.loadcsv;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvLoader {

	private final static String DELIMITER = ",";
	private static CsvLoader loader;

	public static CsvLoader getInstance() {
		if (loader == null) {
			loader = new CsvLoader();
		}

		return loader;
	}

	private CsvLoader() {

	}

	private List<String> optLine(String[] masString) {

		List<String> row = new ArrayList<>();

		for (int i = 0; i < masString.length; i++) {
			String str = "";
			int strOptSize = masString[i].replaceAll("[\"]", "").length();
			// The situation where quotes are opened and closed in one line
			if ((masString[i].indexOf("\"") != -1) && ((masString[i].length() - strOptSize) % 2 > 0)) {
				do {
					str = str + masString[i] + ", ";
					i++;
				} while (i < masString.length && masString[i].indexOf("\"") == -1);
				if (i < masString.length) {
					str = str + masString[i];
				}
			} else {
				str = masString[i];
			}
			row.add(str);
		}

		return row;
	}

	public <T> List<T> load(String filePatch, CsvConverter<T> converter) {
		List<T> resultList = new ArrayList<>();
		String[] masString = null;
		BufferedReader br = null;
		String line = "";

		try {

			br = new BufferedReader(new FileReader(filePatch));

			List<String> headers = Arrays.asList(br.readLine().split(DELIMITER));

			while ((line = br.readLine()) != null) {
				if ((masString = line.split(DELIMITER)).length > 0) {
					List<String> row = optLine(masString);
					resultList.add(converter.convert(headers, row));
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return resultList;
	}

}
