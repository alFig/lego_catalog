package services.loadcsv;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;


public abstract class Converter<T> implements CsvConverter<T> {
	
	public T convertOb(T ob, Map<String, BiConsumer<T, String>> fieldsMapper, List<String> headers, List<String> row) {

		//Sets sets = new Sets();
		if (row.size() == headers.size()) {
			Iterator<String> headerIterator = headers.iterator();
			Iterator<String> rowIterator = row.iterator();
			while (headerIterator.hasNext()) {
				String headerName = headerIterator.next();
				String value = rowIterator.next();
				fieldsMapper.get(headerName.toLowerCase()).accept(ob, value);
			}
		} else {
			// throw new Exception("Error: The number of columns does not match - Id:" +
			// row.get(0));
			//System.out.println("Error: The number of columns does not match - Sets Id:" + row.get(0));
		}

		return ob;
	}

}
