package services.loadcsv;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import services.db.Sets;

public class SetsCsvConverter extends Converter<Sets> {

	private static Map<String, BiConsumer<Sets, String>> fieldsMapper = new HashMap<>();

	// set_num,name,year,theme_id,num_parts
	static {
		fieldsMapper.put("set_num", Sets::setSetNum);
		fieldsMapper.put("name", Sets::setName);
		fieldsMapper.put("year", (object, value) -> object.setYear(Integer.parseInt(value)));
		fieldsMapper.put("theme_id", (object, value) -> object.setThemeId(Integer.parseInt(value)));
		fieldsMapper.put("num_parts", (object, value) -> object.setNumParts(Integer.parseInt(value)));
	}

	public Sets convert(List<String> headers, List<String> row) {

		return convertOb(new Sets(), fieldsMapper, headers, row);
	}

}
