package services.loadcsv;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import services.db.Colors;

public class ColorsCsvConverter<T> extends Converter<Colors> {

	private static Map<String, BiConsumer<Colors, String>> fieldsMapper = new HashMap<>();

	// set_num,name,year,theme_id,num_parts
	static {
		fieldsMapper.put("id", (object, value) -> object.setId(Integer.parseInt(value)));
		fieldsMapper.put("name", Colors::setName);
		fieldsMapper.put("rgb", Colors::setRgb);
		fieldsMapper.put("is_trans", Colors::setIsTrans);
	}

	public Colors convert(List<String> headers, List<String> row) {
		return convertOb(new Colors(), fieldsMapper, headers, row);
	}

}
