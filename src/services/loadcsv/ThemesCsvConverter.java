package services.loadcsv;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import services.db.Themes;

public class ThemesCsvConverter implements CsvConverter<Themes> {

	private static Map<String, BiConsumer<Themes, String>> fieldsMapper = new HashMap<>();

	// set_num,name,year,theme_id,num_parts
	static {
		fieldsMapper.put("id", (object, value) -> object.setId(Integer.parseInt(value)));
		fieldsMapper.put("name", Themes::setName);
		fieldsMapper.put("parent_id", (object, value) -> object.setParentId(Integer.parseInt(value)));
	}

	@Override
	public Themes convert(List<String> headers, List<String> row) {
		// id,name,parent_id
		Themes themes = new Themes();
		
		while(row.size() < headers.size()) {
			row.add("0");
		}
		
		if (row.size() == headers.size()) {
			Iterator<String> headerIterator = headers.iterator();
			Iterator<String> rowIterator = row.iterator();
			while (headerIterator.hasNext()) {
				String headerName = headerIterator.next();
				String value = rowIterator.next();
				fieldsMapper.get(headerName.toLowerCase()).accept(themes, value);
			}
		} else {
			// throw new Exception("Error: The number of columns does not match - Id:" + row.get(0));
			//System.out.println("Error: The number of columns does not match - Themes Id:" + row.get(0));
		}

		return themes;
	}

}
