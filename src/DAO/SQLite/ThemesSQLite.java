package dao.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.DaoException;
import dao.DaoFactory;
import services.db.Themes;

public class ThemesSQLite {
	private DaoFactory daoFactory = DaoFactory.getInstance();

	public void create(Themes themes) throws DaoException {
		String sql = "INSERT INTO themes(id, name, parent_id) VALUES(?,?,?)";

		Connection connection = null;
		PreparedStatement prStatement = null;

		try {
			connection = daoFactory.getConnection();
			prStatement = connection.prepareStatement(sql);
			prStatement.setInt(1, themes.getId());
			prStatement.setString(2, themes.getName());
			prStatement.setInt(3, themes.getParentId());
			prStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prStatement != null) {
				try {
					prStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Themes> selectAll() throws DaoException {
		String sql = "SELECT id, name, parent_id FROM themes";

		List<Themes> list = new ArrayList<Themes>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		ResultSet result = null;
		Themes themes = null;

		try {
			connection = daoFactory.getConnection();
			prStatement = connection.prepareStatement(sql);
			result = prStatement.executeQuery();
			while (result.next()) {
				themes = new Themes(result.getInt("id"), result.getString("name"), result.getInt("parent_id"));
				list.add(themes);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (prStatement != null) {
				try {
					prStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public Themes selectId(int id) throws DaoException {
		String sql = "SELECT id, name, parent_id FROM themes WHERE id = ?";

		Connection connection = null;
		PreparedStatement prStatement = null;
		ResultSet result = null;
		Themes themes = null;

		try {
			connection = daoFactory.getConnection();
			prStatement = connection.prepareStatement(sql);
			prStatement.setInt(1, id);
			result = prStatement.executeQuery();
			while (result.next()) {
				themes = new Themes(result.getInt("id"), result.getString("name"), result.getInt("parent_id"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (prStatement != null) {
				try {
					prStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return themes;

	}

	public boolean createTable(DbTools dbTools) throws DaoException {
		String sql = "CREATE TABLE IF NOT EXISTS themes (\n" 
				+ "id integer PRIMARY KEY,\n" 
				+ "name text NOT NULL,\n"
				+ "parent_id integer \n" + ");";
		return dbTools.runRequest(sql);
	}

	public boolean dropTable(DbTools dbTools) throws DaoException {
		String sql = "DROP TABLE themes";
		return dbTools.runRequest(sql);
	}
}
