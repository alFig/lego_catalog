package dao.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.DaoException;
import dao.DaoFactory;
import services.db.Sets;

public class SetsSQLite {
	DaoFactory daoFactory = DaoFactory.getInstance();

	public void create(Sets sets) throws DaoException {
		String sql = "INSERT INTO sets(set_num, name, year, theme_id, num_parts ) VALUES(?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement prStatement = null;

		try {
			connection = daoFactory.getConnection();
			prStatement = connection.prepareStatement(sql);
			prStatement.setString(1, sets.getSetNum());
			prStatement.setString(2, sets.getName());
			prStatement.setInt(3, sets.getYear());
			prStatement.setInt(4, sets.getThemeId());
			prStatement.setInt(5, sets.getNumParts());
			prStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prStatement != null) {
				try {
					prStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Sets> selectAll() throws DaoException {
		String sql = "SELECT set_num, name, year, theme_id, num_parts FROM sets";

		List<Sets> list = new ArrayList<Sets>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		ResultSet result = null;
		Sets sets = null;

		try {
			connection = daoFactory.getConnection();
			prStatement = connection.prepareStatement(sql);
			result = prStatement.executeQuery();
			while (result.next()) {
				sets = new Sets(result.getString("set_num"), result.getString("name"), result.getInt("year"),
						result.getInt("theme_id"), result.getInt("num_parts"));
				list.add(sets);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (result != null) {
				try {
					result.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (prStatement != null) {
				try {
					prStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return list;
	}
	
	public void createTable(DbTools dbTools) throws DaoException {
		String sql = "CREATE TABLE IF NOT EXISTS sets (\n" 
						+ "set_num text PRIMARY KEY,\n"
						+ "name text NOT NULL, \n" 
						+ "year integer NOT NULL, \n"
						+ "theme_id integer, \n"
						+ "num_parts integer \n"
						+ ");";
		dbTools.runRequest(sql);
	}
	
	public void dropTable(DbTools dbTools) throws DaoException {
		String sql = "DROP TABLE sets";
		if (dbTools.isTableExists("sets")) {
			dbTools.runRequest(sql);
		}
	}

}
