package dao.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.DaoException;
import dao.DaoFactory;

public class DbTools {
	private DaoFactory daoFactory = DaoFactory.getInstance();

	public boolean runRequest(String sql) throws DaoException {

		boolean result = true;
		Connection connection = null;
		PreparedStatement prStatement = null;

		try {
			connection = daoFactory.getConnection();
			prStatement = connection.prepareStatement(sql);
			result = prStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prStatement != null) {
				try {
					prStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return result;
	}
	
	public boolean isTableExists(String name) throws DaoException {
		String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";

		Connection connection = null;
		PreparedStatement prStatement = null;
		ResultSet result = null;
		boolean exists = false;

		try {
			connection = daoFactory.getConnection();
			prStatement = connection.prepareStatement(sql);
			prStatement.setString(1, name);
			result = prStatement.executeQuery();
			exists = result.next();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prStatement != null) {
				try {
					prStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return exists;
	}
}
