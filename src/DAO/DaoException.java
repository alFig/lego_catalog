package dao;

public class DaoException extends Exception {
	
	public DaoException() {
		super();
	}
	
	public DaoException(String message) {
		super(message);
	}
	
	public DaoException(String message, Throwable caus) {
		super(message, caus);
	}

}
