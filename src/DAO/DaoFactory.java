package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import dao.sqlite.SetsSQLite;
import dao.sqlite.ThemesSQLite;

public class DaoFactory {

	private static DaoFactory daoFactory = null;
	private String url = "jdbc:sqlite:legoDB.db";

	public static DaoFactory getInstance() {
		if (daoFactory == null) {
			daoFactory = new DaoFactory();
		}
		return daoFactory;
	}

	public Connection getConnection() throws DaoException {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(url);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return connection;
	}

	public ThemesSQLite getThemesDao() {
		// TODO Auto-generated method stub
		return new ThemesSQLite();
	}

	public SetsSQLite getSetsDao() {
		// TODO Auto-generated method stub
		return new SetsSQLite();
	}

}
