package ui;

import java.util.InputMismatchException;
import java.util.Scanner;

import dao.DaoException;
import services.ShowDbService;

public class MenuShow extends Menu {

	private final String textOption = "1 - Show sets\n2 - Show thems\n3 - Back to main menu";
	private final int maxValue = 3;
	private ShowDbService showDB;
	
	public MenuShow() throws DaoException { 
		showDB = new ShowDbService();		
	}

	@Override
	public void showMenu() {
		int select = 0;

		try {
			Scanner scanner = new Scanner(System.in);
			do {
				select = inputNumberMenu(scanner, textOption, maxValue);
				switch (select) {
				case 1:
					try {
						tablePrint(showDB.showSets());
					} catch (DaoException e){
						getMessageLn(getErrorDB());
						e.printStackTrace();
					}
					waitButtonPres();
					break;
				case 2:
					try {
						tablePrint(showDB.showThems());
					} catch (DaoException e){
						getMessageLn(getErrorDB());
						e.printStackTrace();
					}
					waitButtonPres();
					break;
				}

			} while (select != maxValue);
		} catch (InputMismatchException e) {
			e.printStackTrace();
		}
	}

}
