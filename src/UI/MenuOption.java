package ui;

import java.util.InputMismatchException;
import java.util.Scanner;

import dao.DaoException;
import services.LoadDbService;

public class MenuOption extends Menu {

	private final static String TEXT_OPTION = "1 - Update database\n2 - Re-creation database\n3 - Back to main menu";
	private final static String TEXT_WAITING = "Waiting, loading database";
	private int maxValue = 3;

	private LoadDbService loadDB;

	public MenuOption() {
		try {
			loadDB = new LoadDbService();
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void showMenu() {
		int select = 0;

		try {
			Scanner scanner = new Scanner(System.in);
			do {
				select = inputNumberMenu(scanner, TEXT_OPTION, maxValue);
				switch (select) {
				case 1:

					waitButtonPres();
					break;
				case 2:
					try {
						getMessageLn(TEXT_WAITING);
						loadDB.createDB();
					} catch (DaoException e) {
						getMessageLn(getErrorDB());
						e.printStackTrace();
					}
					waitButtonPres();
					break;
				}

			} while (select != maxValue);
		} catch (InputMismatchException e) {
			e.printStackTrace();
		}
	}

}
