package ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public abstract class Menu {
	private final static String TEXT_CONTINUE = "To continue press any button";
	private final static String TEXT_SELECT = "Please select menu item: ";
	private final static String TEXT_INVALID_INPUT = "Invalid menu item, please try again";
	private final static String TEXT_ERROR_DB = "Error: Operation not performed";
	private final static String TEXT_ERROR_INPUT = "Error: Incorrect value";

	private final static String NEW_LINE = "\n";
	private final static String V_SPLIT_SYMBOL = "|";
	private final static String H_SPLIT_SYMBOL = "-";

	protected abstract void showMenu();
	
	protected int inputNumberMenu(Scanner scanner, String textOption, int maxValue) {
		int value = 0;
		getMessageLn(textOption);
		getMessage(TEXT_SELECT);
		if (scanner.hasNextInt()) {
			value = scanner.nextInt();
		} else {
			scanner.next();
		}

		while (value < 1 || value > maxValue) {
			getMessageLn(TEXT_INVALID_INPUT);
			if (scanner.hasNextInt()) {
				value = scanner.nextInt();
			} else {
				scanner.next();
			}
		}

		return value;
	}
	
	protected int inputNumber(Scanner scanner, String textQuestion) {
		int value = 0;
		do {
			getMessage(textQuestion);
			if (scanner.hasNextInt()) {
				value = scanner.nextInt();
				break;
			} else {
				getMessageLn(TEXT_ERROR_INPUT);
				scanner.next();
			}
		} while (true);

		return value;
	}

private Map<Integer, Integer> getMaxSizeCollumns(List<ArrayList<String>> rowsList) {
		Map<Integer, Integer> maSizeCollumns = new HashMap<Integer, Integer>();

		int lenght = rowsList.get(0).size();
		for (int i = 0; i < lenght; i++) {
			maSizeCollumns.put(i, 0);
		}

		for (ArrayList<String> row : rowsList) {
			for (int i = 0; i < row.size(); i++) {
				int size = row.get(i).length();
				if (maSizeCollumns.get(i) < size) {
					maSizeCollumns.put(i, size);
				}
			}
		}

		return maSizeCollumns;
	}

	private String addSpace(String str, Integer lenght) {

		while (str.length() < lenght) {
			str = str + " ";
		}

		return str;
	}

	private void createHLine(StringBuilder strBuilder, int sizeHead) {
		for (int i = 0; i < sizeHead; i++) {
			strBuilder.append(H_SPLIT_SYMBOL);
		}
		strBuilder.append(NEW_LINE);
	}

	protected void tablePrint(List<ArrayList<String>> rowsList) {
		Map<Integer, Integer> maxSizeCollumns = getMaxSizeCollumns(rowsList);
		StringBuilder strBuilder = new StringBuilder();
		ArrayList<String> row = null;

		int sizeTableH = getSizeTable(maxSizeCollumns);
		createHLine(strBuilder, sizeTableH);

		for (int i = 0; i < rowsList.size(); i++) {
			row = rowsList.get(i);
			for (int j = 0; j < row.size(); j++) {
				if (j == 0) {
					strBuilder.append(V_SPLIT_SYMBOL);
				}
				strBuilder.append(addSpace(row.get(j), maxSizeCollumns.get(j)));
				strBuilder.append(V_SPLIT_SYMBOL);
			}
			strBuilder.append(NEW_LINE);
			if(i == 0){
				createHLine(strBuilder, sizeTableH);				
			}
		}
		createHLine(strBuilder, sizeTableH);

		getMessageLn(strBuilder.toString());
	}

	private int getSizeTable(Map<Integer, Integer> maxSizeCollumns) {
		int sizeTable = 0;

		for (int value : maxSizeCollumns.values()) {
			sizeTable = sizeTable + value;
		}

		return sizeTable;
	}

	protected void waitButtonPres() {
		getMessageLn(TEXT_CONTINUE);
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void getMessage(String str) {
		System.out.print(str);
	}

	protected void getMessageLn(String str) {
		System.out.println(str);
	}

	protected String getTextContinue() {
		return TEXT_CONTINUE;
	}

	protected String getTextSelect() {
		return TEXT_SELECT;
	}

	protected String getTextInvalidInput() {
		return TEXT_INVALID_INPUT;
	}
	
	protected String getErrorDB() {
		return TEXT_ERROR_DB;
	}
}
