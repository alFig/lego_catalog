package ui;

import java.util.Scanner;

import dao.DaoException;

public class MenuMain extends Menu {

	private final String textOption = "1 - Searc\n2 - Show table\n3 - Option\n4 - Exit";
	private final int maxValue = 4;

	private MenuSearch menuSearch;
	private MenuShow menuShow;;
	private MenuOption menuOption;

	public MenuMain() throws DaoException {
		menuSearch = new MenuSearch();
		menuShow = new MenuShow();
		menuOption = new MenuOption();
	}

	@Override
	public void showMenu() {
		int select = 0;

		try (Scanner scanner = new Scanner(System.in)) {
			do {
				select = inputNumberMenu(scanner, textOption, maxValue);
				switch (select) {
				case 1:
					menuSearch.showMenu();
					break;
				case 2:
					menuShow.showMenu();
					break;
				case 3:
					menuOption.showMenu();
					break;
				}

			} while (select != maxValue);
		}
	}

}
